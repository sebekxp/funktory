#include "SimpleIntervalExecutor.h"
#include <iostream>
#include <thread>
#include <algorithm>
#include <cmath>
#include <ctime>
#include <iomanip>
#include <memory>

void SimpleIntervalExecutor::register_f(std::function<void(std::chrono::system_clock::time_point)> fun,
                                        std::chrono::milliseconds interval) {
    queue.emplace_back(fun, interval);
}

void SimpleIntervalExecutor::run() {

    std::sort(queue.begin(), queue.end(), [](auto &left, auto &right) {
        return left.second < right.second;
    });

    auto duration = std::chrono::milliseconds(0);
    bool flag = true;
    for (auto &it : queue) {
        if(flag){
            std::this_thread::sleep_for(std::chrono::milliseconds(it.second));
            it.first(std::chrono::system_clock::now());
            flag = false;
        }else{
            if(duration < (&it + 1)->second)
                std::this_thread::sleep_for(std::chrono::milliseconds((&it + 1)->second - it.second));

            auto t1 = std::chrono::high_resolution_clock::now();
            it.first(std::chrono::system_clock::now());
            auto t2 = std::chrono::high_resolution_clock::now();

            duration = std::chrono::duration_cast<std::chrono::milliseconds>( t2 - t1 );
        }

    }
}
