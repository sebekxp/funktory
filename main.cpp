#include <iostream>
#include "SimpleIntervalExecutor.h"
#include <iomanip> // put_time
#include <thread>

typedef std::chrono::milliseconds mls;

void fun(std::chrono::system_clock::time_point time){

    auto in_time_t = std::chrono::system_clock::to_time_t(time);
    std::cout << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X")<<std::endl;
}


//void test(std::chrono::system_clock::time_point time){
//
//    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
//    std::cout << "aaa" << std::endl;
//}

int main() {
    SimpleIntervalExecutor obj;

    auto a_fun = fun;

    auto lambda_1 = [](std::chrono::system_clock::time_point) { std::cout<<"Lambda 1"<<std::endl; };
    auto lambda_2 = [](std::chrono::system_clock::time_point) { std::cout<<"Lambda 2"<<std::endl; };
    auto lambda_3 = [](std::chrono::system_clock::time_point) { std::cout<<"Lambda 3"<<std::endl; };
    auto lambda_4 = [](std::chrono::system_clock::time_point) { std::cout<<"Lambda 4"<<std::endl; };
    auto lambda_5 = [](std::chrono::system_clock::time_point) { std::cout<<"Lambda 5"<<std::endl; };
    auto lambda_6 = [](std::chrono::system_clock::time_point) { std::cout<<"Lambda 6"<<std::endl; };
    auto lambda_7 = [](std::chrono::system_clock::time_point) { std::cout<<"Lambda 7"<<std::endl; };
    auto lambda_8 = [](std::chrono::system_clock::time_point) { std::cout<<"Lambda 8"<<std::endl; };

    Useless funktor_1("Funktor 1");
    Useless funktor_2("Funktor 2");
    Useless funktor_3("Funktor 3");
    Useless funktor_4("Funktor 4");
    Useless funktor_5("Funktor 5");
    Useless funktor_6("Funktor 6");
    Useless funktor_7("Funktor 7");
    Useless funktor_8("Funktor 8");

//    funktor_7(std::chrono::system_clock::now());
//    funktor_7(std::chrono::system_clock::now());
//    funktor_7(std::chrono::system_clock::now());
//    funktor_7(std::chrono::system_clock::now());
    //obj.register_f(test,mls(500));
    obj.register_f(a_fun,mls(600));
    obj.register_f(lambda_1,mls(1500));
    obj.register_f(lambda_2,mls(1300));
    obj.register_f(lambda_3,mls(10000));
    obj.register_f(lambda_4,mls(9000));
    obj.register_f(lambda_5,mls(8000));
    obj.register_f(lambda_6,mls(7000));
    obj.register_f(lambda_7,mls(6000));
    obj.register_f(lambda_8,mls(4500));
    obj.register_f(funktor_1,mls(3500));
    obj.register_f(funktor_2,mls(3000));
    obj.register_f(funktor_3,mls(2500));
    obj.register_f(funktor_4,mls(2000));
    obj.register_f(funktor_5,mls(1500));
    obj.register_f(funktor_6,mls(1000));

    /* Test zliczania funktora */
    obj.register_f(funktor_7,mls(500));
    obj.register_f(funktor_7,mls(500));
    obj.register_f(funktor_7,mls(500));
    obj.register_f(funktor_7,mls(500));
    obj.register_f(funktor_7,mls(500));
    obj.register_f(funktor_7,mls(500));
    obj.register_f(funktor_7,mls(500));
    obj.register_f(funktor_7,mls(500));
    obj.register_f(funktor_7,mls(500));
    obj.register_f(funktor_7,mls(500));
    obj.register_f(funktor_8,mls(0));

    obj.run();

    return 0;
}