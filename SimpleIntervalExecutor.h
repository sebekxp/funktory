#ifndef FUNKTORY_SIMPLEINTERVALEXECUTOR_H
#define FUNKTORY_SIMPLEINTERVALEXECUTOR_H

#include <chrono>
#include <functional>
#include <vector>
#include <iostream>
#include <memory>
class SimpleIntervalExecutor {
public:
    void register_f(std::function<void(std::chrono::system_clock::time_point)>,
                    std::chrono::milliseconds interval);

    void run();

private:
    std::vector<std::pair<std::function<void(std::chrono::system_clock::time_point)>, std::chrono::milliseconds>> queue;
};

class Useless {
public:
    explicit Useless(std::string str) : nameFun(std::move(str)) {}

    void operator()(std::chrono::system_clock::time_point){
        std::cout << nameFun<< ": " << count++<< std::endl;
    }
private:
    std::string nameFun;
    int count = 0;
};

#endif //FUNKTORY_SIMPLEINTERVALEXECUTOR_H
